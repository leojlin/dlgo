import six.moves.cPickle as pickle
import gzip
import numpy as np

def encode_label(j):
    e = np.zeros((10, 1))
    e[j] = 1.0
    return e

def shape_data(data):
    features = [np.reshape(x, (784, 1)) for x in data[0]]
    labels = [encode_label(y) for y in data[1]]
    return list(zip(features, labels))

def load_data_impl():
    path = 'mnist.npz'
    file = np.load(path)
    x_train, y_train = file['x_train'], file['y_train']
    x_test, y_test = file['x_train'], file['y_train']
    file.close
    return (x_train, y_train), (x_test, y_test)

def load_data():
    train_data, test_data = load_data_impl()
    return shape_data(train_data), shape_data(test_data)
