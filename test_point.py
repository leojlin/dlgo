import unittest 
from unittest import TestCase
from dlgo.gotypes import Point

class TestPoint(TestCase):
    def test_neighbors(self):
        point = Point(2,2)
        neighbors = point.neighbors()
        self.assertIn(Point(1,2), neighbors)
        self.assertIn(Point(3,2), neighbors)
        self.assertIn(Point(2,1), neighbors)
        self.assertIn(Point(2,3), neighbors)
        self.assertNotIn(point, neighbors)
        self.assertNotIn(Point(1,1), neighbors)

if __name__ == '__main__':
    unittest.main()