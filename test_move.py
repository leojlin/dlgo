import unittest 
from unittest import TestCase
from dlgo.goboard_slow import Move
from dlgo.gotypes import Point

class TestMove(TestCase):
    def test_play(self):
        point = Point(1,1)
        self.assertEqual(Move.play(point).point, Point(1,1))

    def test_pass(self):
        self.assertTrue(Move.pass_turn().is_pass)

    def test_resign(self):
        self.assertTrue(Move.resign().is_resign)

if __name__ == '__main__':
    unittest.main()